CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Configuration
 * Maintainers


INTRODUCTION
------------

This module provides a block integration with Elelvio Assistant:

 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/elevio
 * To submit bug reports and feature suggestions or to track changes, either use
   the issue tracker on Drupal.org or GitHub:
   - https://www.drupal.org/project/issues/elevio
   - https://github.com/drupal-modules/elevio


INSTALLATION
------------

 * Install the Elevio module as you would normally install a contributed
   Drupal module. Visit [Installing Drupal Modules](https://www.drupal.org/node/1897420) for further
   information.


CONFIGURATION
-------------

 1. Navigate to Administration > Extend and enable the module.
 2. Login to your Elevio account, Navigate to Settings > Installation and Copy the Account ID & Account Secret.
 3. Navigate to Administration > Configuration > Web services > Elelvio
    for configurations.
 4. Fill in the Account ID & Account Secret you copied from Elevio.
 5. Go to /admin/structure/block/list/TARGET_THEME and place the block "Elevio Assistant block"


MAINTAINERS
-----------

 * Hawwari (mhawwari) - https://www.drupal.org/u/mhawwari

### Supporting organizations:

 * [Vardot](https://www.drupal.org/vardot) - Development and maintenance
