<?php

namespace Drupal\elevio\Plugin\Block;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;

/**
 * Provides a 'Elevio Assistant' block.
 *
 * @Block(
 *   id = "elevio_assistant_block",
 *   admin_label = @Translation("Elevio Assistant block"),
 *   category = @Translation("Elevio")
 * )
 */
class ElevioAssistantBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Current Account Interface.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $account;


  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, ConfigFactoryInterface $config_factory, AccountInterface $account) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->configFactory = $config_factory;
    $this->account = $account;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('config.factory'),
      $container->get('current_user')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    // Get Account id & secret.
    $config = $this->configFactory->get('elevio.config');
    $account_id = $config->get('account_id');
    $account_secret = $config->get('account_secret');
    if ($account_id && $account_secret) {
      // Check if user is not anonymous to include user data.
      if (!$this->account->isAnonymous()) {
        $user = $this->account->getAccount();
        return [
          '#theme' => 'elevio_assistant_block',
          '#account_id' => $account_id,
          '#username' => $user->getAccountName(),
          '#email' => $user->getEmail(),
          '#user_hash' => hash_hmac("sha256", $user->getEmail(), $account_secret),
        ];
      }
      else {
        return [
          '#theme' => 'elevio_assistant_block',
          '#account_id' => $account_id,
          '#username' => 'Anonymous',
        ];
      }
    }
    else {
      return [
        '#theme' => 'markup',
        '#markup' => 'Elevio isn\'t configured yet',
      ];
    }

  }

  /**
   * {@inheritdoc}
   */
  protected function blockAccess(AccountInterface $account) {
    if ($account->hasPermission('access elevio assistant')) {
      return AccessResult::allowed();
    }
    else {
      return AccessResult::forbidden();
    }
  }

}
