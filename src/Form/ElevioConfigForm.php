<?php

namespace Drupal\elevio\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides Elevio admin configuration form.
 */
class ElevioConfigForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'elevio_admin';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'elevio.config',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('elevio.config');

    $form['account_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Account id'),
      '#description' => $this->t('Account id provided from the Elevio installation page.'),
      '#default_value' => $config->get('account_id'),
    ];

    $form['account_secret'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Account secret'),
      '#description' => $this->t('Secret key provided from the Elevio installation page.'),
      '#default_value' => $config->get('account_secret'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->config('elevio.config')
      ->set('account_id', $form_state->getValue('account_id'))
      ->set('account_secret', $form_state->getValue('account_secret'))
      ->save();
  }

}
